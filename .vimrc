set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-surround'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'bronson/vim-trailing-whitespace'
Plugin 'kshenoy/vim-signature'
Plugin 'Raimondi/delimitMate'
Plugin 'tomasr/molokai'
Plugin 'tpope/vim-fugitive'
Plugin 'ClockworkNet/vim-apparmor'
Plugin 'nvie/vim-flake8'
Plugin 'xsbeats/vim-blade'
Plugin 'mustache/vim-mustache-handlebars'
Plugin 'ciaranm/detectindent'

if hostname() == "anthony-dev"
    Plugin 'Valloric/YouCompleteMe'
    let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
    Plugin 'majutsushi/tagbar'
    let g:Tlist_Inc_Winwidth = 1
endif

Plugin 'scrooloose/nerdcommenter'
let g:NERDSpaceDelims = 1

Plugin 'marijnh/tern_for_vim'
let g:tern#command = ["nodejs", expand('<sfile>:h') . '/.vim/bundle/tern_for_vim/node_modules/tern/bin/tern', '--no-port-file']

Plugin 'wesleyche/SrcExpl'
let g:SrcExpl_pluginList = [
    \ "Source_Explorer",
    \ "__Tagbar__",
    \ "NERD_tree_*"
    \ ]
let g:SrcExpl_isUpdateTags = 0

Plugin 'syntastic'
let g:syntastic_check_on_open = 1

Plugin 'ctrlp.vim'
let g:ctrlp_match_window_bottom = 0
let g:ctrlp_show_hidden = 1
let g:ctrlp_follow_symlinks = 1
let g:ctrlp_extensions = ['buffertag']
let g:ctrlp_custom_ignore  = {
    \ 'dir': '\.git$\|vendor$'
    \ }

Plugin 'vim-airline'
set laststatus=2
set showtabline=2
set noshowmode
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#tab_nr_type = 1
let g:airline#extensions#tabline#show_tab_type = 0
let g:airline#extensions#tabline#fnamemod = ':p:t'
let g:airline#extensions#tabline#show_close_button = 0

Plugin 'altercation/vim-colors-solarized'
set background=dark

Plugin 'Lokaltog/vim-easymotion'
let g:EasyMotion_smartcase = 1
map <Leader> <Plug>(easymotion-prefix)
map <Leader>l <Plug>(easymotion-lineforward)
map <Leader>h <Plug>(easymotion-linebackward)

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
" ---------------------------------------

set fileencodings=utf-8,big5,gbk,latin1
set fileencoding=utf-8
syntax on
set t_Co=256
set hlsearch
set autoindent
set gfn=Consolas:h14
set nobackup "不要自動儲存備份
set backspace=2 "backspace刪除
set ruler "顯示狀態
set showmatch "高亮度顯示匹配括號
set number
set cursorline
set tabstop=4
set shiftwidth=4
set list
set listchars=tab:»·
set expandtab
set colorcolumn=80
set tabpagemax=50
set diffopt=vertical,filler
colorscheme solarized

inoremap <silent> <F5> <Esc>:NERDTree<CR>
inoremap <silent> <F4> <Esc>:TagbarToggle<CR>
inoremap <C-l> <Esc>:tabnext<CR>
inoremap <C-h> <Esc>:tabprevious<CR>
inoremap <C-s> <Esc>:w<CR>
inoremap <F7> <Esc>:tabm -1<CR>
inoremap <F8> <Esc>:tabm +1<CR>
inoremap <C-r> <Esc>:CtrlPBufTag<CR>
inoremap <silent> <F9> <Esc>:SrcExplToggle<CR>

nnoremap <silent> <F5> :NERDTree<CR>
nnoremap <silent> <F4> :TagbarToggle<CR>
nnoremap <C-l> :tabnext<CR>
nnoremap <C-h> :tabprevious<CR>
nnoremap <C-s> :w<CR>
nnoremap <F7> :tabm -1<CR>
nnoremap <F8> :tabm +1<CR>
nnoremap <C-r> :CtrlPBufTag<CR>
nnoremap <silent> <F9> :SrcExplToggle<CR>

let g:lasttab = 1
nnoremap <tab> :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()
autocmd TabEnter,WinEnter,VimEnter,BufEnter * DetectIndent
autocmd BufNewFile,BufRead resource setf json
com! PrettifyJSON %!python -m json.tool

let pwd = getcwd()
let tags_dir = ''
let base_dirs = [
    \ '/synosrc/curr/ds.base',
    \ '/synosrc/DSM5-2-2015Q1/ds.base',
    \ '/synosrc/SRM-master/ds.base',
    \ '/synosrc/DSM6-0/ds.base',
    \ '/synosrc/pkg',
    \ ]
for i in base_dirs
    if pwd =~ i
        let tags_dir = i
    endif
endfor
if strlen(tags_dir) > 0
    let tags_dir .= "/tags"
    let tag_files = split(globpath(tags_dir, '*'), '\n')
    for tag_file in tag_files
        let &tags .= ',' . tag_file
    endfor
endif
