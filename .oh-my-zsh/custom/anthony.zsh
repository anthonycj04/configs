# Add yourself some shortcuts to projects you often work on
# Example:
#
# brainstormr=/Users/robbyrussell/Projects/development/planetargon/brainstormr
#
alias la='ls -a'
alias ll='ls -lA'
alias ls='ls -F --color=auto'
alias back='tmux attach -d 2> /dev/null || tmux new'
alias off='sudo halt -p now'
alias reboot='sudo reboot'
alias vim='vim -p'
alias mond='umount /DiskStation; mount -o user=syno,passwd=,nounix //192.168.252.5/DiskStation /DiskStation'
alias mons='umount /snoopy; mount -o user=syno,passwd=,nounix //192.168.252.21/public /snoopy'
alias monw='lsof -i :2233 2>&1 > /dev/null; [ $? -ne 0 ] && ssh -N -f -L 2222:localhost:2049 -L 2233:localhost:892 -p 1220 root@nas.aya92.com; mkdir -p /web; mount -o port=2222,mountport=2233,tcp localhost:/volume1/web /web'
alias pkginfo='/synosrc/pkg/pkgscripts-ng/include/release_mail_config.php --list detail'
#export GREP_OPTIONS='--color=auto'
export TMPDIR=/tmp
[ -d "$HOME/bin" ] && export PATH="$PATH:$HOME/bin"

stty -ixon

case "$(hostname)" in
	anthony-dev)
		alias sshaxp='ssh root@10.12.12.99'
		alias sshbuildpkg='ssh dit@buildpkg2.synology.com'
		alias j=jump
		source "$(jump-bin --zsh-integration)"
		BnI () {
			platform="$1"
			project="$2"
			update="$3"
			if [ -z "$update" ]; then
				./BuildAll -UFp "$platform" "$project" && ./BuildAll -BiFp "$platform" "$project"
			else
				./BuildAll -Fp "$platform" "$project" && ./BuildAll -BiFp "$platform" "$project"
			fi
		}
		;;
	anthony-nas)
		;;
	anthony-dev-vm)
		;;
	anthony-syno-mint)
		;;
esac

POWERLINE_SHOW_GIT_ON_RIGHT="true"
POWERLINE_RIGHT_A="exit-status"

getPlatformModelMapping() {
	local keyword="$1"

	result="$(cat /synosrc/curr/ds.base/source/sds-custom/platform.prf | grep "\[.*\]" | cut -c 11- | cut -d']' -f1 | grep "$keyword")"
	if [ "$(echo $result | wc -l)" -gt 1 ]; then
		echo "$(echo $result | grep "_$keyword")"
	else
		echo "$result"
	fi
}

queryPkgList() {
	local machine="$1"
	local build_number="$2"
	local major=6
	local minor=1
	local unique="synology_$(getPlatformModelMapping $machine)"
	local arch="$(echo $unique | cut -d_ -f2)"

	curl -L "https://payment.synology.com/api/getPackageList.php?package_update_channel=stable&unique=$unique&build=$build_number&language=cht&major=$major&arch=$arch&minor=$minor&timezone=Taipei" | jq .
}
